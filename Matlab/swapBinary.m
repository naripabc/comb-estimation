function new_dec = swapBinary(dec,N)
    %% converts a decimal number to a binary array, swap the binary array
    %% such that big-endian is converted to low-endian, converts it back to
    %% a decimal number.
    %
    % input
    % ---------------------------------------------------------------------
    % dec:      decimal number
    % N:        number of bits necessary to represent dec as binary
    %
    % output
    % ---------------------------------------------------------------------
    % new_dec:  converted binary array
    
    dec = dec - 1;
    binary = zeros(N,1);
    for i=1:N
        binary(i) = mod(dec,2);
        dec = dec - binary(i);
        dec = dec / 2;
    end
    new_bin = flipud(binary);
    new_dec = 0;
    for i=1:N
        new_dec = new_dec + 2^(i-1)*new_bin(i);
    end
    new_dec = new_dec + 1;

end