function [QFI,T,Cder] = primalProblem(Ctheta, theta_value, dimensions, ...
                                dpur, h, daux, verbose)
    %% solves the primal problem of finding the QFI.
    %
    % input
    % ---------------------------------------------------------------------
    % Ctheta:       ensemble decomposition, shape [dim, comp] where dim is
    %               the dimension of the Choi operator of the comb and comp
    %               is the number of components in the ensemble
    %               decomposition.
    % theta_value:  numerical value of theta.
    % dimensions:   dimensions of the Hilbert spaces H_1, ..., H_{2N}.
    % dpur:         dimension of the purifying system.
    % h:            minimising Hermitian h from the dual problem. If not
    %               provided, the function solves the dual problem and
    %               finds h.
    % verbose:      takes values 0,1,2,3.
    %
    % output
    % ---------------------------------------------------------------------
    % QFI:          QFI of the primal problem
    % T:            maximising tester
    % Cder:         the operator Cder that appear in the primal problem.
    
    arguments
        Ctheta = 0;
        theta_value = 2*pi/20;
        dimensions = [2,2,2,2];
        dpur = 1;
        h = -1000;
        daux = 2;
        verbose = 3;
    end
    % if Cder is not initialised
    if Ctheta == 0
        disp("primalProblem: create Ntheta")
        syms tau g t theta;
        var = [tau, g, t, theta];
        var_values = [0.5, 1, 0.5, 2*pi/20];
        gate = "swap";
        N = 2;
        Ctheta = nonMarkovControl(gate, N, var, var_values);
    end
    
    % dpur does not exist, but auxiliary space
    dpur = 1;
    twoN = length(dimensions);
    N = twoN / 2;
    dimT = zeros(N,1);
    for i=1:N
        dimT(i) = dpur * prod( dimensions(1:2*i-1) );
    end
    dimT(N) = dimT(N) * daux;
    if h == -1000
        disp("primalProblem: find h via dual")
        Cder = createCder(Ctheta, dimensions, theta_value);
    else
        Cder = createCder(Ctheta, dimensions, theta_value, h);
    end    
        
    cvx_begin sdp quiet  
        if N >= 1
            dimSubComb = dimT(1);
            variable T1(dimSubComb,dimSubComb) semidefinite;
            b{1} = T1;
        end
        if N >= 2
            dimSubComb = dimT(2);
            variable T2(dimSubComb,dimSubComb) semidefinite;
            b{2} = T2;
        end
        if N>= 3
            dimSubComb = dimT(3);
            variable T3(dimSubComb,dimSubComb) semidefinite;
            b{3} = T3;
        end
        if N>= 4
            dimSubComb = dimT(4);
            variable T4(dimSubComb,dimSubComb) semidefinite;
            b{4} = T4;
        end
        if N  >= 5
            dimSubComb = dimT(5);
            variable T5(dimSubComb,dimSubComb) semidefinite;
            b{5} = T5;
        end
        if N >= 6
            errID = 'MyError:NotImplemented';
            msg = "N>= 6 is not yet implemented. To implement, uncomment the lines where the exception was thrown.";
            notImplementedException = MException(errID, msg);    
            throw(notImplementedException);
            % dimSubComb = dimT(6);
            % variable T6(dimSubComb,dimSubComb) semidefinite;
            % b{6} = T6;
        end
            
        trace(b{1}) == 1;
        for k=2:N-1
            % tr_{2k-1} becomes a trace over system 2k, since system 1 is
            % the purifying system.
            PartialTrace(b{k},2*k-1,dimensions(1:2*k-1)) == ...
                 kron(b{k-1}, eye(dimensions(2*k-2)));
        end
        if N > 1
            PartialTrace(b{N},[2*N-1,2*N], [dimensions(1:2*N-1) daux]) == ...
                kron(b{N-1}, eye(dimensions(2*N-2)));
        end
        maximise( real(trace( ...
                PartialTrace(b{N},2*N, [dimensions(1:2*N-1) daux]) ...
                    * Cder ) ) );
          
    cvx_end
    
    QFI = 4 * cvx_optval;
    T = full(b{N});
      
end
