function U_controlfree = cnot_system(tau, g, t, theta, copies)
    %% computes the unitary if the interaction is generated by a CNOT,
    %% where the system acts as a control over the environment.
    U_controlfree = sym(zeros(4,4));
    U_controlfree = [exp(-1j*g*tau), 0, 0, 0;
        0, exp(-1j*theta*t)*cos(g*tau), 0, -1j*sin(g*tau)*exp(-1j*theta*t);
        0, 0, exp(-1j*g*tau), 0;
        0, -1j*sin(g*tau)*exp(-1j*theta*t), 0, cos(g*tau)*exp(-1j*theta*t)];


end