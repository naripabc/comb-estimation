function [QFI,h] = minimalQFI(Ctheta, theta_value, dimensions, dpur)
    %% solves the dual problem.
    %
    % input
    % ---------------------------------------------------------------------
    % Ctheta:       ensemble decomposition, shape [dim, comp] where dim is
    %               the dimension of the Choi operator of the comb and comp
    %               is the number of components in the ensemble
    % theta_value:  numerical value of theta.
    % dimensions:   dimensions of the Hilbert spaces H_1, ..., H_{2N}.
    % dpur:         dimension of the purifying system.
    %
    % output
    % ---------------------------------------------------------------------
    % QFI:          QFI of the dual problem
    % h:            minimising Hermitian h.
    
    arguments
        Ctheta = 0;
        theta_value = 0.1;
        dimensions = [2,2,2,2];
        dpur = 1;
    end
    % if Ctheta is not initialised
    if Ctheta == 0
        disp("Ctheta not initialised in minimalQFI")
        syms theta;
        Ctheta = sym(zeros(16,2));
        Ctheta(:,1) = [exp(-2i), 0, 0, cos(1)*exp(-theta*1i)*exp(-1i), ...
             0, 0, -exp(-theta*1i)*sin(1)^2, 0, ...
             0, 0, 0, 0, ...
             cos(1)*exp(-theta*1i)*exp(-1i), 0, 0, cos(1)^2*exp(-theta*2i)];
        
        Ctheta(:,2) = [0, exp(-theta*1i)*exp(-1i)*sin(1)*1i, 0, 0, ...
            cos(1)*exp(-theta*1i)*sin(1)*1i, 0, ...
                0, exp(-theta*1i)*exp(- theta*1i - 1i)*sin(1)*1i, ...
            0, 0, 0, 0, 0, ...
            cos(1)*exp(-theta*2i)*sin(1)*1i, 0, 0];
    end
    
    twoN = length(dimensions);
    N = twoN / 2;
    dim2N = dimensions(end); % dimension of H_2N
    dimSk = zeros(N-1); % dimensions of S^{(k)}
    for i=1:N-1
        dimSk(i) = prod(dimensions(1:2*i)) * dpur;
    end
    dimC = size(Ctheta);
    sizeC = dimC(1);
    components = dimC(2);
    Cdot = diff(Ctheta);
    Cdot = double(subs(Cdot, theta_value));
    Ctheta = double(subs(Ctheta, theta_value));
    
    cvx_begin sdp quiet
        % optimise over equivalent ensemble operators
        % variable h_diag(components);
        %expression h(components, components);
        %h = diag(h_diag);
        variable h(components,components) hermitian;
        expression Cdottilde(sizeC,components);
        
        % create Cdottilde
        for i=1:components
             Cdottilde(:,i) = Cdot(:,i) - 1j*Ctheta * h(:,i);
        end
        
        % build matrix A
        dimCder = prod(dimensions(1:end-1))*dpur; % length of second block
        length1 = components*dim2N*dpur; % length of first block
        totalLength = dimCder + length1;
        expression A(totalLength, totalLength);
        A(1:length1,1:length1) = eye(length1);
        for i=1:components
            for k=1:dim2N
                % only uper right left, the lower left is determined by
                % the hermiticity of A.
                row_idx = dpur*(dim2N*(i-1) + (k-1)) + 1;
                A(row_idx:row_idx+dpur-1,(length1+1):totalLength) = ...
                   kron(eye(dpur), transpose(Cdottilde(k:dim2N:end,i)));
            end
        end
        A((length1+1):totalLength, 1:length1) = ...
                                    A(1:length1, (length1+1):totalLength)';
        
        % optimize over different testers S
        variable lambda nonnegative;
        if N == 1
            % last step in optimising A
            A((length1+1):totalLength,(length1+1):totalLength) = ...
                            lambda*sparse(eye(dimensions(end-1) * dpur));
            A >= 0;
        else
            if N >= 2
                variable S1(dimSk(1),dimSk(1)) complex semidefinite;
                S{1} = S1;
            end
            if N >= 3
                variable S2(dimSk(2),dimSk(2)) complex semidefinite;
                S{2} = S2;
            end
            if N >= 4
                variable S3(dimSk(3),dimSk(3)) complex semidefinite;
                S{3} = S3;
            end
            if N >= 5
                variable S4(dimSk(4),dimSk(4)) complex semidefinite;
                S{4} = S4;
            end
             if N >= 6
                variable S5(dimSk(5),dimSk(5)) complex semidefinite;
                S{5} = S5;
            end
            if N >= 7
                errID = 'MyError:NotImplemented';
                msg = "N>= 7 is not yet implemented. To implement, uncomment the lines where the exception was thrown.";
                notImplementedException = MException(errID, msg);    
                throw(notImplementedException);
                %  variable S6(dimSk(6),dimSk(6)) hermitian;
                % S{6} = S6;
            end
                
            PartialTrace(S{1},3,[dpur, dimensions(1:2)]) == ...
                                lambda * sparse(eye(dimensions(1) * dpur));
            for k=2:N-1
                dimk = dimensions(1:2*k);
                PartialTrace(S{k},2*k+1, [dpur, dimk]) == ...
                                kron(S{k-1},sparse(eye(dimensions(2*k-1))));
            end
            % last step in optimising A
            A((length1+1):totalLength,(length1+1):totalLength) = ...
                                kron(S{N-1},sparse(eye(dimensions(end-1))));
            A >= 0;
        end
     
        minimize( lambda );
    cvx_end
   
%     Cder = createCder(Ctheta2, dimensions, theta_value, h, dpur) / 21.444
%     Cder = Cdottilde * Cdottilde';
%     time = linspace(0,21,196);
%     t = time(66) / 2;
%     C_nor = PartialTrace(Cder, 4, [2,2,2,2]) / (4*t);
%     diag(real(C_nor))'
%     
    % smoothen numerical instabilities
    for k=1:N-1
        dims = size(S{k});
        for i=1:dims(1)
            for j=1:dims(2)
                if abs(S{k}(i,j)) < 0.001
                    S{k}(i,j) = 0;
                end
            end
        end
    end
    
    S = 0;
    if N == 2
        S = S1;
    elseif N == 3
        S = S2;
    elseif N == 4
        S = S3;
    elseif N == 5
        S = S4;
    elseif N ==6
        S = S5;
    end
    

    
    QFI = 4 * cvx_optval;
end