function store_dat = simulation(N, iter, option)
    %% calculates the quantum Fisher information (QFI) for a quantum N-comb,
    %% for a number of iterations. saves the outputs and creates a plot.
    %
    % input
    % ---------------------------------------------------------------------
    % N: number of system-environment interactions.
    % iter: number of times where the QFI is calculated.
    % option: optional parameters
    % option.gate:  generator of the system-environment interaction,
    %               available gates are 'swap', 'cnot_environment'
    %               and 'cnot_system'.
    % option.var:   variables used in the interaction. Default is [tau, g,
    %               t, theta]
    % option.reset: If true, calculate the QFI, if not, only plot data.
    % option.verbose: output information about progress of calculation.
    %               Possible values are 0,1,2,3,4, the higher the value,
    %               the more is output.
    % option.plot_data: If true, plot data, if false only calculate data.
    % option.save_folder: Folder, where plot is saved.
    % option.parallel: If true, parallelise computation. Parallel Computing
    %               Toolbox is needed.
    % option.workers: number of workers used for parallel computation.
    % option.interaction: determines the strength of the interaction.
    %               Available interactions are 'identity' (tau = t), 
    %               'square' (tau = t^2), 'sqrt' (tau = sqrt(t).
    % option.g:     set the value of g.
    % option.theta: set the value of theta, in the paper called omega.
    % option.dpur:  set the dimension of the purifying system H_{tilde{1}}.
    % option.save:  If true, save the plot and the data acquired in
    %               save_folder.
    % option.primal: If true, solve the primal problem for the
    %               non-Markovian setting with control.
    % option.collisionOnly: If true, only calculate the QFI for the 
    %               non-Markovian setting with control.
    % 
    % output
    % ---------------------------------------------------------------------
    % store_dat:	location of the csv file with generated data. 
    
    arguments
        N double = 2;
        iter double = 4;
        option.gate = 'swap'; % generator of the system-environment interaction
        option.var = 0;
        option.reset = true;
        option.verbose = 3;    
        option.plot_data = 1;
        option.save_folder = "/home/anian/polybox/Masterthesis/Simulation/";
        option.parallel = 0;
        option.workers = 24;
        option.interaction = "identity";
        option.g = 1;
        option.theta = 0.05 * 2*pi;
        option.dpur double = 1;
        option.save = 1;
        option.primal = 1;
        option.collisionOnly = 0;
        option.daux = 1
        option.copies = 1;
    end

    theta_value = option.theta;
    dpur = option.dpur;
    t_end = 1.05 * 2*pi / theta_value;
    start = datetime;
    
    % interesting variables: N, iter, gate, interaction, theta_value
    
    if option.var == 0
        syms t real;
        syms g real;
        syms theta real;
        syms tau real;
        option.var = [tau, g, t, theta];
    end
    
    % When running on the supercomputer, use different save folder and
    % parallelisation options.
    supercomputer = 0;
    if contains(pwd, 'cluster')
        supercomputer = 1;
        % run /cluster/home/aaltherr/Documents/MATLAB/startup.m
        option.save_folder = "/cluster/home/aaltherr/Simulation/";
    end   
    
    % display all the options
    if option.verbose > 0
        N 
        iter
        for x=option
            disp(x)
        end
    end
    
    % allocate empty arrays
    t_tot = linspace(0,t_end,iter);
    [U, U_controlfree, U_Markov, U_Markov_controlfree] ...
            = createUnitaries(tau,g,t,theta, option.gate, option.copies);
  
    if option.reset
        QFI = zeros(1,iter);
        QFI_no_control = zeros(1,iter);
        QFI_Markov = zeros(1,iter);
        QFI_Markov_controlfree = zeros(1,iter);
        QFI_Heisenberg = zeros(1,iter);
        QFI_primal = zeros(1,iter);
        if option.gate == "bitflip"
            % number of components is equal dim(E)^2
            h = zeros(iter, 2^(2), 2^(2));
        elseif option.gate == "multipleBitflip"
            h = zeros(iter, 2^(2*option.copies), 2^(2*option.copies));
        else
            h = zeros(iter, 2, 2);
        end
        T = zeros(iter, dpur*2^(option.copies*(2*N-1)), dpur*2^(option.copies*(2*N-1)));
        
        interaction = str2func(option.interaction);
        variables = option.var;
        verbosity = option.verbose;
        var_array = zeros(4,iter);
        for i=1:iter
         % var = [t, g, theta];
            ti = t_tot(i) / N;
            if option.interaction == "identity"
                var_array(:,i) = [ti, option.g, ti, theta_value];
            elseif option.interaction == "none"
                var_array(:,i) = [ti, 0, ti, theta_value];                
            else
                var_array(:,i) = [interaction(ti), option.g, ti, theta_value];
            end
        end

        tau = var_array(1,:);
        gi = var_array(2,:);
        time = var_array(3,:);
        thetai = var_array(4,:);
        primal = option.primal;
        collisionOnly = option.collisionOnly;
        gate = option.gate;
        daux = option.daux;
        copies = option.copies;
        
        % calculate QFI for different times in parallel.
        if option.parallel
            if supercomputer
                % parallel.importProfile('/cluster/apps/matlab/support/EulerLSF8h.settings')
                batch_job = parcluster('EulerLSF8h');
            else
                batch_job = parcluster('local');
            end
            pool = parpool(batch_job, option.workers);
                        
            parfor i=1:iter
                [QFI(i), QFI_no_control(i), QFI_Markov(i), QFI_Markov_controlfree(i), ...
                    QFI_Heisenberg(i), QFI_primal(i), h(i,:,:), T(i,:,:)] = ...
                    simulateTimeStep(i, N, dpur, variables, tau, gi, time, thetai, ...
                        U, U_controlfree, U_Markov, U_Markov_controlfree, verbosity, ...
                        primal, collisionOnly, gate, daux, copies);

            end
            pool.delete();
        else
            for i=1:iter
                [QFI(i), QFI_no_control(i), QFI_Markov(i), QFI_Markov_controlfree(i), ...
                    QFI_Heisenberg(i), QFI_primal(i), h(i,:,:), T(i,:,:)] = ...
                    simulateTimeStep(i, N, dpur, variables, tau, gi, time, thetai, ...
                        U, U_controlfree, U_Markov, U_Markov_controlfree, verbosity, ...
                        primal, collisionOnly, gate, daux, copies);
            end
        end
        
        % store data
        store_dat = option.save_folder + "Data/" + option.gate + "-" + option.interaction + "-N-" + N + "_" + createLabel(start);
        fileID = fopen(store_dat + '.csv','w');
        info = "%N: " + num2str(N) + ", iterations: " + num2str(iter);
        info = info + "gate: " +  option.gate + ", theta: " + num2str(option.theta);
        info = info + ", interaction: " + option.interaction + ", theta: " + num2str(option.theta);
        info = info + ", dpur: " + num2str(dpur);
        fprintf(fileID, "%s \n", info);
        labels =  ["% t_tot "; "QFI     "; "QFI_no_control"; "QFI_Markov"; "QFI_Markov_controlfree"; "QFI_Heisenberg"];
        fprintf(fileID, "   ");
        fprintf(fileID, "%s \t \t", labels);
        fprintf(fileID, "\n");
        QFI_array = transpose([t_tot; QFI; QFI_no_control; QFI_Markov; QFI_Markov_controlfree; QFI_Heisenberg]);
        save(store_dat + ".csv", "QFI_array" , "-ascii", "-append");
        
        fileInfo = fopen(store_dat + '_info.txt', 'w');
        info = "%N: " + num2str(N) + ", iterations: " + num2str(iter);
        fprintf(fileInfo, "%s \n", info);
        fprintf(fileInfo, "%s \n", struct2str(option));
        save(store_dat + "_T.mat", "T");
        save(store_dat + "_h.mat", "h");
        
         
    % if option.reset = 0, load data from file.    
    else
       filepattern = option.save_folder + "Data/" + option.gate + "-" + option.interaction + "-N-" + N + "_*";
       [status, filename] = system("ls " + filepattern + " -t | head -n 1");
       filename = filename(1:end-1); % truncate end of line character
       data = load(filename, "-ascii");
       data = transpose(data);
       t_tot = data(1,:);
       QFI = data(2,:);
       QFI_no_control = data(3,:);
       QFI_Markov = data(4,:);
       QFI_Markov_controlfree = data(5,:);
       QFI_Heisenberg = data(6,:);
    end

    % plot data
    if option.plot_data
        set(0, 'defaulttextinterpreter', 'latex');
        if option.save
            f = figure('visible', 'off');
        else
            f = figure();
        end
        p1 = plot(t_tot,QFI, 'r');
        hold on;
        p2 = plot(t_tot,QFI_no_control, 'r--'); % 714
        hold on;
        p3 = plot(t_tot,QFI_Markov, 'b');
        hold on;
        p4 = plot(t_tot, QFI_Markov_controlfree, 'b--');
        hold on;
        p5 = plot(t_tot, QFI_Heisenberg, 'k--');
        hold off;
        xlabel('$t_{tot}$', 'Interpreter', 'latex');
        ylabel('$J_Q(C_{\theta})$', 'Interpreter', 'latex');
        set(gca, 'TickLabelInterpreter','latex');
        fraction = sprintf('%g',theta_value / (2*pi) );
        title(option.gate + ", $N = " + num2str(N) + "$", 'Interpreter', 'latex');
        legend(["non-Markov control", "non-Markov no control", "Markov control", ...
                "Markov no control", "noiseless limit"], 'Location', 'northwest', 'Interpreter', 'latex');
        store = option.save_folder + "/Plots/" + option.gate + "-" + option.interaction + "-N-" + N + "_" + createLabel(start);      
        if option.save
            exportgraphics(f,store + ".pdf",'ContentType','vector');
            close;
        end
    end
    
    end_time = datetime;
    if option.verbose > 0
        elapsed_time = end_time - start
    end
end
