function [Ctheta,dimensions] = nonMarkovControl(gate, N, var, var_values, ...
            initial_state, copies, verbose)
    %% calculates the QFI for the non-Markovian case with control.
    %
    % input
    % ---------------------------------------------------------------------
    % gate:     generator of the interaction (see simulation.m)
    % N:        order of quantum N-comb
    % var:      variables, default [tau, g, t, theta]
    % var_values:  numerical values of var.
    % verbose:  takes values 0,1,2,3.
    %
    % output
    % ---------------------------------------------------------------------
    % C_swap:   ensemble decomposition of comb with dimensions
    %           [dim, comp] where dim is the dimension of Choi operator of
    %           the comb and comp is the number of components in the
    %           ensemble decomposition.
    % dimensions: array of the dimensions of the Hilbert spaces H_1, ...,
    %           H_{2N}
        
    % variables should be of the form [tau, g, t, theta]
    
    arguments
        gate = "swap";
        N = 2;
        var = 0;
        var_values = [0.5,1,0.5,2*pi/20];
        initial_state = "zero"; % or maximal_mixed
        copies = 1;
        verbose = 3;
    end
    
    if var == 0
        disp('creating U for swap in collisionModel')
        syms tau g t theta;
        var = [tau, g, t, theta];
    end
    
    createU = str2func(gate);
    U_controlfree = createU(var(1),var(2),var(3),var(4), copies);
    shape = size(U_controlfree);
    U = controlfreeToControl(U_controlfree);
    Utot = U; % see below for createU
    for i=2:N
        % create next U and concatenate, iterate N-1 times.
        Utot = specialMultiplication(U, Utot);
    end
    dimensions = sqrt(shape(1))*ones(1,2*N);
    totalDim = prod(dimensions);

    % compare equation
    if initial_state == "zero"
        Ctheta = sym(zeros(totalDim,2));
        Ctheta(:,1) = reshape(Utot(1,1,:),1,[]);
        Ctheta(:,2) = reshape(Utot(2,1,:),1,[]);
    elseif initial_state == "maximal_mixed"
        dimUtot = size(Utot);
        dimE = dimUtot(1);
        components = dimE^2;
        Ctheta = sym(zeros(totalDim,components));
        for i=1:dimE
            for j=1:dimE
                Ctheta(:,dimE*(i-1)+j) = reshape(Utot(j,i,:),1,[]);
            end
        end
        Ctheta = Ctheta / sqrt(dimE); % account for initial state normalisation 
    end
    
    
    if verbose >= 4
        time = var_values(1)
    end
    for i=1:length(var)-1
        Ctheta = subs(Ctheta,var(i),var_values(i));
    end
    
    % Ctheta has coefficients (k_2N, ..., k_2, k_1), but we want them in
    % the order (k_1, k_2, ..., k_2N).
    sizeCtheta = size(Ctheta);
    C_swap = sym(zeros(sizeCtheta(1),sizeCtheta(2)));
    bits = log2(totalDim);
    for i=1:totalDim
        i_new = swapBinary(i,bits);
        C_swap(i_new,:) = Ctheta(i,:);
    end
    Ctheta = C_swap;
    
end