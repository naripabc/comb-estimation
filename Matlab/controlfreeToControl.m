function U = controlfreeToControl(U_controlfree)
    %% converts a unitary for the non-Markovian process without control to 
    %% a unitary for the non-Markovian process with control.
    sizeU = size(U_controlfree);
    % assume that environment and system have same dimension
    dimS = sqrt(sizeU(1));
    dimE = dimS;
    U = sym(zeros(dimE,dimE,dimS*dimS));
    for i = 1:dimE
        for j = 1:dimE
            x1 = dimS*(i-1);
            y1 = dimS*(j-1);
            U(i,j,:) = ...
             reshape(transpose(U_controlfree(x1+1:x1+dimS,y1+1:y1+dimS)), 1, []);
        end
    end
end