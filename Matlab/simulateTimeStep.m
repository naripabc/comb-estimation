function [QFI_dual, QFI_no_control, QFI_Markov, QFI_Markov_controlfree, QFI_Heisenberg, ...
                QFI_primal, h, T] = ...
            simulateTimeStep(i, N, dpur, variables, tau, gi, time, thetai, ...
            U, U_controlfree, U_Markov, U_Markov_controlfree, verbose, ...
            primal, collisionOnly, gate, daux, copies)
    %% calculates the QFI for one time step. only use it conjunction with
    %% simulation.m, for testing use simulateStep.m
    %
    % input
    % ---------------------------------------------------------------------
    %
    % i:        iteration corresponding to time step i.
    % N:        defines the quantum N-comb.
    % dpur:     dimension of the purifying system.
    % variables: by default [tau, g, t, theta].
    % tau:      interaction time between system and environment.
    % gi:       interaction strength between system and environment.
    % time:     evolution time of the system.
    % thetai:   frequency of the system's evolution.
    % U:        unitary for non-Markovian setting with control.
    % U_controlfree: unitary for non-Markovian setting without control.
    % U_Markov: unitary for Markovian setting with control.
    % U_Markov_controlfree: unitary for Markovian setting without control.
    % verbose:  output intermediary results, possible values are 0,1,2,3.
    % primal:   If true, solves primal problem for non-Markovian setting
    %           with control.
    % collisionOnly: If true, only solve non-Markovian setting with
    %           control.
    % gate: generator of the interaction, available options are 'swap',
    %           'cnot_environment', 'cnot_system'.
    
    % output
    % ---------------------------------------------------------------------
    % QFI:      QFI for the different settings: 
    % QFI_dual: non-Markovian with control, dual problem
    % QFI_no_control:   non-Markovian without control
    % QFI_Markov: Markovian with control
    % QFI_Markov_controlfree:   Markovian without control
    % QFI_Heisenberg:   noiseless setting (g=0).
    % QFI_primal:  non-Markovian with control, primal problem.
    % h:        minimising Hermitian.
    % T:        optimal tester.
      
    % var = [tau, g, t, theta];
    
    if gate == "bitflip" || gate == "multipleBitflip"
        %initial_state = "zero";
        initial_state = "maximal_mixed";
    else
        initial_state = "zero";
    end
    
    var_values = [tau(i), gi(i), time(i), thetai(i)];
    [Ctheta, dimensions] = nonMarkovControl(gate, N, variables, ...
                               var_values, initial_state, copies, verbose);                                
    theta_value = var_values(end);
    [QFI_dual,h] = minimalQFI(Ctheta, theta_value, dimensions, dpur);

    QFI_primal = 0;
    T = 0;
    if primal
        [QFI_primal,T] = primalProblem(Ctheta, theta_value, ...
                            dimensions, dpur, h, daux);
        
    % find Ctheta for g = 0
    [Ctheta, dimensions] = nonMarkovControl(gate, N, variables, ...
            [var_values(1), 0, var_values(3), var_values(4)], ...
            initial_state, copies, verbose);
    QFI_Heisenberg = minimalQFI(Ctheta, var_values(4), dimensions, dpur); 
    end
    
    if collisionOnly == 0
        QFI_no_control = nonMarkovNoControl(U_controlfree, N, ...
                               variables, var_values, initial_state, copies, dpur);
        if gate == "multipleBitflip" && copies > 1
            % exceeds computational power of my laptop
            QFI_Markov = 0;
        else
            QFI_Markov = markovControl(U_Markov, N, ...
                                            variables, var_values, dpur);
        end
        QFI_Markov_controlfree = markovNoControl(U_Markov_controlfree, ...
                                        N, variables, var_values, dpur);
        
    else
        QFI_no_control = 0;
        QFI_Markov = 0;
        QFI_Markov_controlfree = 0;
    end

    if verbose == 1 && mod(i,30) == 0
        disp(i)
    elseif verbose == 2 && mod(i,10) == 0
        disp(i)
    elseif verbose >= 3
        disp(i)
        [QFI_Heisenberg, QFI_dual, QFI_primal, QFI_no_control, QFI_Markov, ...
        QFI_Markov_controlfree]/copies^2
    end
    if verbose >= 4
        disp(T)
    end
end