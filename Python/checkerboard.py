import pennylane as qml
import numpy as np
import pandas as pd
from Comb import Comb
from PartialSwap import PartialSwap


def checkerboard(N, interaction, theta=None, g=None):
    """creates a Pennylane quantum circuit for the checkerboard ansatz, 
        see https://arxiv.org/pdf/1906.10155v2.
        
    Parameters
    ----------
    N : int, optional
        number of teeth for the comb (Default 2)
    theta : double, optional
        parameter to be estimated (Default np.pi/5)
    g : double, optional
        interaction strength (Default 0.0)

    Returns 
    -------
    comb_check : instance of class Comb
    """
    if theta == None:
        theta = np.pi/5
    if g == None:
        g = 0.0
    np.random.seed(13)
    dof = 5 * (N+1)
    var = np.random.random(dof)
    var = 4*np.pi*var - 2*np.pi

    def circuit(interaction, var, theta, g, N):
        # state initialisation
        qml.RX(var[0], wires='system')
        qml.RX(var[1], wires='anc')
        qml.MultiRZ(var[2], wires=['anc','system'])
        qml.RZ(var[3], wires='system')
        qml.RZ(var[4], wires='anc')
        for i in range(1,N+1):
            # comb
            qml.PhaseShift(-theta, wires='system')
            interaction(g, wires=['system', 'env'])
            # control operation
            qml.RX(var[5*i], wires='system')
            qml.RX(var[5*i+1], wires='anc')
            qml.MultiRZ(var[5*i+2], wires=['anc','system'])
            qml.RZ(var[5*i+3], wires='system')
            qml.RZ(var[5*i+4], wires='anc')
        
        return qml.probs(['anc','system'])

    comb_check = Comb(N, interaction, theta, g, dof, circuit)

    # var_opt_check = comb_check.optimize(var, iterations=10, eta=2, adaptive=True)
    return comb_check 
