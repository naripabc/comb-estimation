import pennylane as qml
import numpy as np
from datetime import datetime
import pandas as pd
import subprocess
import sys


class Comb:
    """class used to represent a quantum comb.

    ...
    Attributes
    ----------
    N : int
        number of teeth of comb
    interaction : function
        interaction between system and environment given as Pennylane two-qubit gate
    theta : double
        parameter to be estimated, frequency of evolution, corresponds to theta * t in Matlab setting
    g : double
        interaction strength times evolution time in interaction, corresponds to g * tau in Matlab setting
    dof : double
        degree of freedoms, number of parameters for ansatz
    circuit_function: function
        creates a quantum circuit as a function with the variational parameters as arguments
    dev : pennylane.device
        Device for quantum circuit
    circuit : pennylane.QNode
        Quantum Node 

    Methods
    -------
    assertion(var)
        asserts that number of parameters (dof) equals the length of var

    prob(var)
        calculates the probabilities for measurement outcomes

    draw_circuit(var)
        draws the circuit

    grad_theta(var)
        computes the gradient of prob w.r.t. to theta

    grad_var(theta)
        computes the gradient of prob w.r.t. to the variational parameters

    QFI(psi, psidot)
        computes the quantum Fisher information for a psi and its derivative

    qfi_circuit(var)
        computes the QFI of a circuit

    gradient_qfi(var)
        computes the gradient of QFI w.r.t. to (theta, var)

    optimize(var, iterations=None, eta=None, adaptive=None)
        initialises a gradient descent to maximise the QFI

    adap_learn(var, t_tot=None, opt_step=5, parallel=0, qfi_comb_simple=None)
        optimises the parameters with an adaptive gradient descent and for different times

    """
    def __init__(self, N, interaction, theta, g, dof, circuit_function):
        self.N = N
        self.interaction = interaction
        self.theta = theta
        self.g = g
        self.dof = dof # degree of freedom
        self.circuit_function = lambda var, theta, g: circuit_function(self.interaction, var, theta, g, self.N)
        
        self.dev = qml.device('default.qubit', wires=['anc','system','env'])
        self.dev.operations.add(self.interaction.name)
        self.circuit = qml.QNode(self.circuit_function, self.dev, diff_method="parameter-shift")
    
    def assertion(self, var):
        no_of_var = len(var)
        assert len(var) == self.dof, "{} variables needed, {} given.".format(no_of_var, len(var))
        
    def prob(self, var):
        self.assertion(var)
        psi = self.circuit(var, self.theta, self.g)
        return psi
    
    def draw_circuit(self, var):
        self.assertion(var)
        self.circuit(var, self.theta, self.g)
        print(self.circuit.draw())
    
    def grad_theta(self, var):        
        self.assertion(var)
        gradient_theta = qml.jacobian(lambda theta: self.circuit(var, theta, self.g))
        return gradient_theta(self.theta)
    
    def grad_var(self, var):        
        self.assertion(var)
        gradient_var = qml.jacobian(lambda var: self.circuit(var, self.theta, self.g))
        return gradient_var(var)
    
    def QFI(psi, psidot):
        return sum([psidot[i]**2 / psi[i] for i in np.flatnonzero(psi)])

    def qfi_circuit(self, var):
        self.assertion(var)
        return Comb.QFI(self.prob(var), self.grad_theta(var)) / (self.N)**2
    
    def gradient_qfi(self, var, indices=None):
        number_of_parameters = len(var)
        if type(indices) != list and type(indices) != np.ndarray:
            indices = range(number_of_parameters)
        self.assertion(var)
        p = self.prob(var)
        params = np.concatenate((self.theta, var), axis=None)
        gradient_params = qml.jacobian(lambda params: self.circuit(params[1:], params[0], g))
        gradient_theta = self.grad_theta(var)
        gradient_var = self.grad_var(var)
        gradient_var_theta = np.zeros((*gradient_theta.shape, number_of_parameters)) 
        for k in indices:
            delta = np.zeros(number_of_parameters)
            delta[k] = np.pi/2
            gradient_var_theta[:,k] = 0.5 * (self.grad_theta(var+delta) - self.grad_theta(var-delta))
    
        grad_QFI = np.zeros(number_of_parameters)
        for k in indices:
            for i in np.flatnonzero(p):
                grad_QFI[k] += 2 * gradient_theta[i] * gradient_var_theta[i,k] / p[i]
                grad_QFI[k] -= gradient_theta[i]**2 * gradient_var[i,k] / p[i]**2
    
        return grad_QFI
    
    def optimize(self, var, iterations=None, eta=None, adaptive=None):
        var_opt = gradient_descent(var, self.qfi_circuit, 
                            self.gradient_qfi, iterations, eta, adaptive, None)
        return var_opt
    
    def adap_learn(self, var, t_tot=None, opt_step=5, qfi_comb_simple=None):
        return adaptive_learning(self, var, opt_step, t_tot, qfi_comb_simple)


def gradient_descent(var, func, gradient, iterations, eta, adaptive, indices, verbose=1):
    """performs gradient descent to maximise the QFI of a given comb
    
    Parameters
    ----------
    var : array_like
        variational parameters
    func : func(var)
        function to be maximised
    gradient: gradient(var)
        gradient of func w.r.t. to 'var'
    iterations : int
        number of iterations of gradient descent
    eta : double
        learning rate
    adpative : deprecated
    verbose : int
        output information for verbose == 1

    Returns
    -------
    var : array_like
        optimised variational parameters
    """
    if iterations == None:
        iterations = 10
    if eta == None:
        eta = 0.1
    if adaptive == None:
        adaptive = False

    f_old = func(var)
    f_before_old = f_old
    f_new = f_old
    var_new = var
    np.random.seed(13)
    if verbose:
        print(0, f_old, eta)
    for i in range(iterations):
        # update var
        var_new = var + eta * gradient(var, indices)
        f_new = func(var_new)
        if verbose:
            print(i+1,f_new, eta)
            sys.stdout.flush()

        if abs(f_before_old - f_old) < 0.001 and abs(f_new - f_old) < 0.001:
            # converged
            if i > 3:
                break
        if f_new - f_old < - 0.2 * f_old:
            if f_old - f_before_old < -0.2 * f_before_old:
                # accept step with probability p=0.1
                if np.random.random() < 0.1:
                    f_before_old = f_old
                    f_old = f_new
                    var = var_new
                else:
                    eta = 0.5 * eta
            else:
                eta = 0.5 * eta
        else:
            # accept step
            f_before_old = f_old
            f_old = f_new
            var = var_new

    return var


def qfi_simple(comb, var, t_tot=None):
    """ computes QFI with parameters var for t_tot
    """
    if t_tot.all() == None:
        t_tot = np.linspace(0,21,101)
    iter = len(t_tot)
    qfi_comb_simple = np.zeros(iter)
    omega = np.pi / 10

    for i in range(iter):
        comb.theta = omega * t_tot[i] / comb.N
        comb.g = t_tot[i] / comb.N
        qfi_comb_simple[i] = comb.qfi_circuit(var)
    return qfi_comb_simple

def adaptive_learning(comb, var_opt_check, opt_step=5, t_tot=None, qfi_comb_simple=None):
    """optimises the parameters for different times

    Parameters
    ----------
    comb : instance of class Comb
    var_opt_check : array_like
        initial values for parameters
    opt_step : int, optional
        number of optimisation steps (default is 5)
    t_tot: array_like, optional
        times for which optimisation is carried out (default is np.linspace(0,21,100))
    qfi_comb_simple: array_like
        QFI for initial configuration var_opt_check (default is None and initialised below)

    Returns
    -------
    var_opt_array : array_like (len(t_tot), number_of_parameters)
        optimised values for parameters
    qfi_comb_opt : array_like (len(t_tot),)
        optimised QFI with var_opt_array
    """
    if t_tot.all() == None:
        t_tot = np.linspace(0,21,100)
    iter = len(t_tot)
    if qfi_comb_simple == None:
        qfi_comb_simple = qfi_simple(comb, var_opt_check, t_tot)
    qfi_comb_opt = np.zeros(iter)
    omega = np.pi / 10

    var_opt_array = np.zeros((iter, *var_opt_check.shape))

    for i in range(iter):
        var_opt_array[i,:], qfi_comb_opt[i] = adaptive_step(i, comb, qfi_comb_simple, var_opt_check, omega, opt_step, t_tot[i])

    return var_opt_array, qfi_comb_opt
    
def adaptive_step(i, comb, qfi_comb_simple, var_opt_check, omega, opt_step, ti):
    """computes single time step of adaptive optimisation
    """
    print(i, ti)
    comb.theta = omega * ti / comb.N
    comb.g = ti / comb.N
    adaptive = False
    if qfi_comb_simple[i] > 0.6:
        eta = 0.05
    elif qfi_comb_simple[i] > 0.4:
        eta = 0.1
    elif qfi_comb_simple[i] > 0.24:
        eta = 0.2
    elif qfi_comb_simple[i] > 0.12:
        eta = 0.3    
    else:
        eta = 0.5
        adaptive = True          
    var_opt_array = comb.optimize(var_opt_check, iterations=opt_step, eta=eta, adaptive=adaptive)
    qfi_comb_opt = comb.qfi_circuit(var_opt_array)
    
    return var_opt_array, qfi_comb_opt


def save_data(t_tot, qfi, name, dir=None):
    """saves the generated QFI data into a csv file
    
    Parameters
    ----------
    t_tot : array_like
        array containing total times
    qfi : array_like
        array containing QFI
    name : string
        characterises the data, here qfi
    dir : string, optional
        where to save the data (default see below)
    """
    if dir == None:
        dir = "/home/anian/polybox/Masterthesis/Simulation/Data/Variational/"
    timestamp = datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
    dict = {"t": t_tot, "qfi_variational": qfi}
    df = pd.DataFrame(dict)
    filename = dir + name + "_" + timestamp + ".csv"
    df.to_csv(filename, index=None)

def load_file(dir, name, string):
    """loads a csv file into a numpy array
    
    Parameters
    ----------
    dir : string
        directory
    name : string
        characterises the data type: e.g. qfi, var
    string : string
        contains gate, interaction, N: e.g. swap-identity-N-2

    Returns
    -------
    df.to_numpy() : array_like
        csv file as numpy array    
    """
    # https://stackoverflow.com/questions/4256107/running-bash-commands-in-python
    filepattern = dir + name + "_" + string + "*.csv"
    command = "ls " + filepattern + " -t | head -n 1" 
    file = subprocess.check_output(command, shell=True)
    file = file[:-1].decode()
    df = pd.read_csv(file)
    return df.to_numpy()

def save_file(dict, dir, name, string, timestamp):
    """saves a Pandas dictionary into a csv file
    """
    df = pd.DataFrame(dict)
    df.to_csv(dir+name+"_"+string+"_"+timestamp+".csv", index=None)

