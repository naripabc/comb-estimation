from pennylane.operation import Operation
import math, cmath
import numpy as np


""" Define a custom two-qubit gate, the partial Swap inspired by
    https://pennylane.readthedocs.io/en/stable/_modules/pennylane/ops/qubit.html.
    We can define custom gates, but we need to add them 
    to the allowed operations of the device: """
# dev.operations.add('myOperation')


class PartialSwap(Operation):
    r"""PartialSWAP(phi, wires)
    The partial swap operator

    Args:
        phi: phase shift
        wires (Sequence[int] or int): the wires the operation acts on
    """
    num_params = 1
    num_wires = 2
    par_domain = "R"
    grad_method = "A"
    name = "PartialSwap"

    @classmethod
    def _matrix(cls, *params):
        phi = params[0]
        e = cmath.exp(-1j*phi)
        c = math.cos(phi)
        s = math.sin(phi)
        
        return np.array([
            [e, 0, 0, 0],
            [0, c, 1j*s, 0],
            [0, 1j*s, c, 0],
            [0, 0, 0, e]
        ])

